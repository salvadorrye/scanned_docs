from django.apps import AppConfig


class ScannedDocsConfig(AppConfig):
    name = 'scanned_docs'

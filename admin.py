from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import Document, Category

class ScannedDocsAdminSite(AdminSite):
    site_header = 'JHWC Scanned Docs Admin'
    site_title = 'JHWC Scanned Docs Admin Portal'
    index_title = 'Welcome to JHWC Scanned Docs Portal'

scanned_docs_admin_site = ScannedDocsAdminSite(name='scanned_docs_admin')

# Register your models here.
@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    search_fields = ['admission__resident__last_name', 'admission__resident__given_name', 'name']
    list_display = ['name', 'admission', 'date', 'category', 'remarks']
    list_filter = ['date', 'category']

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}

scanned_docs_admin_site.register(Document, admin_class=DocumentAdmin)
scanned_docs_admin_site.register(Category, admin_class=CategoryAdmin)

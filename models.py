from django.db import models
from geriatrics.models import Admission

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'
        
    def __str__(self):
        return self.name

class Document(models.Model):
    admission = models.ForeignKey(Admission, on_delete=models.CASCADE)
    date = models.DateField(auto_now=False, null=False, blank=False)
    name = models.CharField(max_length=200, db_index=True)
    category = models.ForeignKey(Category, related_name='documents',
                                 on_delete=models.CASCADE)
    document = models.ImageField(upload_to='documents/%Y/%m/%d/', blank=True)
    remarks = models.TextField(blank=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'document'
    
    def __str__(self):
        return f'{self.name} of {self.admission} on {self.date}'

